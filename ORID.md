O(Objective):
+ I learned the development mode of combining OOP and TDD. 
+ Through learning in the group, the understanding of Strategy pattern is preliminarily formed. 
+ In the afternoon practice, I became more proficient in using IDEA shortcut keys than before,which improved my efficiency in programming practice. 
+ The afternoon practice made me more clear about the classification of classes in development, which will help me implement the requirements I will encounter in the future.

R(Reflective):  
&emsp;&emsp;Fruitful

I(Interpretive):  
&emsp;&emsp;From this afternoon's exercise, I learned that some of my development habits are detrimental to code scalability. In the future, I will pay more attention to code scalability.&emsp;Have a clearer understanding of the concepts and programming methods of TDD and OOP.&emsp;Through practice, I become more proficient in the application of the JAVA stream API.

D(Decision):  
&emsp;&emsp;Continue to learn and understand the Strategy pattern, and continue to practice the Stream API and TDD programming methods.&emsp;Continue to understand OOP and enhance my ability to break down classes when facing requirements.
