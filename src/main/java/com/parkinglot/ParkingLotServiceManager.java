package com.parkinglot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ParkingLotServiceManager extends ParkingBoy{
    private final List<ParkingBoy> managingParkingBoys = new ArrayList<>();

    public ParkingLotServiceManager(ParkingLot... parkingLots) {
        super(parkingLots);
    }

    public List<ParkingBoy> getManagingParkingBoys() {
        return this.managingParkingBoys;
    }

    public void addManagedParkingBoy(ParkingBoy parkingBoy) {
        this.managingParkingBoys.add(parkingBoy);
    }

    public ParkingTicket specifyParkingBoyPark(int parkingBoyNumber, Car car) {
        return this.getManagingParkingBoys().get(parkingBoyNumber).helpParkCar(car);
    }

    public Car specifyParkingBoyFetch(ParkingTicket ticket) {
        return this.managingParkingBoys.stream()
                .filter(parkingBoy ->
                        Arrays.stream(parkingBoy.getParkingLots())
                                .anyMatch(parkingLot -> parkingLot.getParkedCars().stream()
                                        .anyMatch(car -> car.equals(ticket.getCar()))))
                .findFirst().orElseThrow(UnrecognizedParkingTicketException::new)
                .helpFetchCar(ticket);
    }
}
