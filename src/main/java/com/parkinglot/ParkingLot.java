package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class ParkingLot {
    private final List<Car> parkedCars = new ArrayList<>();
    private final int parkSpace;

    public ParkingLot() {
        this.parkSpace = 10;
    }
    public ParkingLot(int parkSpace) {this.parkSpace = parkSpace;}

    public ParkingTicket park(Car car) {

        if (parkedCars.size() < parkSpace) {
            this.parkedCars.add(car);
            return new ParkingTicket(car);

        } else {
            throw new NoAvailablePositionException();

        }

    }

    public Car fetch(ParkingTicket ticket) {
        if (!ticket.getIsUsed()) {
            ticket.setUsed();

            Car fetchedCar = parkedCars.stream().filter(car -> car.equals(ticket.getCar())).findFirst().orElse(null);

            if (fetchedCar != null) {
                parkedCars.remove(fetchedCar);
            } else {
                throw new UnrecognizedParkingTicketException();
            }

            return fetchedCar;

        } else {
            throw new UnrecognizedParkingTicketException();

        }

    }

    public int getParkSpace() {
        return parkSpace;
    }

    public List<Car> getParkedCars() {
        return parkedCars;
    }
}
