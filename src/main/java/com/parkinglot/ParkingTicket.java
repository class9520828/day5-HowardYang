package com.parkinglot;

public class ParkingTicket {
    private Car car = null;
    private boolean isUsed = false;

    public ParkingTicket(Car car) {
        this.car = car;
    }

    public Car getCar() {
        return car;
    }

    public void setUsed() {
        this.isUsed = true;
    }

    public boolean getIsUsed() {
        return this.isUsed;
    }
}
