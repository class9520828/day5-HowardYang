package com.parkinglot;

import java.util.Arrays;

public class ParkingBoy {

    private final ParkingLot[] parkingLots;

    public ParkingBoy(ParkingLot... parkingLots) {
        this.parkingLots = parkingLots;
    }

    public ParkingTicket helpParkCar(Car car) {

        return Arrays.stream(this.parkingLots)
                .filter(parkingLot -> parkingLot.getParkedCars().size() < parkingLot.getParkSpace())
                .findFirst().orElseThrow(NoAvailablePositionException::new).park(car);

    }

    public Car helpFetchCar(ParkingTicket ticket) {

        return Arrays.stream(this.parkingLots)
                .filter(parkingLot -> parkingLot.getParkedCars().stream()
                        .anyMatch(car -> car.equals(ticket.getCar()))).findFirst()
                .orElseThrow(UnrecognizedParkingTicketException::new).fetch(ticket);

    }

    public ParkingLot[] getParkingLots() {
        return parkingLots;
    }
}
