package com.parkinglot;

import java.util.Arrays;
import java.util.Comparator;

public class SuperSmartParkingBoy extends ParkingBoy {
    public SuperSmartParkingBoy(ParkingLot... parkingLots) {
        super(parkingLots);
    }

    @Override
    public ParkingTicket helpParkCar(Car car) {
        return Arrays.stream(this.getParkingLots())
                .filter(parkingLot -> parkingLot.getParkedCars().size() < parkingLot.getParkSpace())
                .max(Comparator.comparingDouble(parkingLot ->
                        (((float) parkingLot.getParkSpace() - (float) parkingLot.getParkedCars().size())
                                / (float) parkingLot.getParkSpace())))
                .orElseThrow(NoAvailablePositionException::new).park(car);

    }
}
