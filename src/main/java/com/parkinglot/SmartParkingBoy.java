package com.parkinglot;

import java.util.Arrays;
import java.util.Comparator;

public class SmartParkingBoy extends ParkingBoy {
    public SmartParkingBoy(ParkingLot... parkingLots) {
        super(parkingLots);

    }

    @Override
    public ParkingTicket helpParkCar(Car car) {

        return Arrays.stream(this.getParkingLots())
                .filter(parkingLot -> parkingLot.getParkedCars().size() < parkingLot.getParkSpace())
                .max(Comparator.comparingInt(parkingLot -> parkingLot.getParkSpace() - parkingLot.getParkedCars().size()))
                .orElseThrow(NoAvailablePositionException::new).park(car);

    }

}
