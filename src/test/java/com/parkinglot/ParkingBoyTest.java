package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingBoyTest {
    @Test
    void should_return_ticket_when_park_given_parking_lot_and_parking_boy_and_car() {
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot());
        Car car = new Car(1);
        ParkingTicket ticket = parkingBoy.helpParkCar(car);

        assertNotNull(ticket);

    }

    @Test
    void should_return_car_when_fetch_given_parking_lot_and_parking_boy_and_ticket() {

        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot());
        Car car = new Car(1);
        ParkingTicket ticket = parkingBoy.helpParkCar(car);

        Car fetchedCar = parkingBoy.helpFetchCar(ticket);
        assertNotNull(fetchedCar);

    }

    @Test
    void should_return_car_when_fetch_given_parking_lot_and_parking_boy_and_two_tickets() {
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot());
        //given
        Car car_1 = new Car(1);
        Car car_2 = new Car(2);
        ParkingTicket ticket_1 = parkingBoy.helpParkCar(car_1);
        ParkingTicket ticket_2 = parkingBoy.helpParkCar(car_2);

        //when
        Car fetched_car_1 = parkingBoy.helpFetchCar(ticket_1);
        Car fetched_car_2 = parkingBoy.helpFetchCar(ticket_2);

        //then
        assertEquals(car_1, fetched_car_1);
        assertEquals(car_2, fetched_car_2);

    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_lot_and_parking_boy_and_wrong_ticket() {
        //given

        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot());
        Car car = new Car(1);
        ParkingTicket wrong_ticket = new ParkingTicket(null);
        parkingBoy.helpParkCar(car);

        //then
        var exception = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingBoy.helpFetchCar(wrong_ticket));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());

    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_lot_and_parking_boy_and_used_ticket() {
        //given

        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot());
        Car car = new Car(1);
        ParkingTicket usedTicket = new ParkingTicket(car);
        parkingBoy.helpParkCar(car);
        parkingBoy.helpFetchCar(usedTicket);

        //then
        var exception = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingBoy.helpFetchCar(usedTicket));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());

    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_full_parking_lot_and_parking_boy_and_car() {
        //given
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot());
        for (int i = 1; i <= parkingBoy.getParkingLots()[0].getParkSpace(); i++) {
            parkingBoy.helpParkCar(new Car(i));
        }
        //then
        var exception = assertThrows(NoAvailablePositionException.class, () -> parkingBoy.helpParkCar(new Car(11)));
        assertEquals("No available position.", exception.getMessage());
    }

    @Test
    void should_return_ticket_when_park_given_two_parking_lots_and_parking_boy_and_car_first_parking_lot_available() {
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot(), new ParkingLot());

        parkingBoy.helpParkCar(new Car(1));
        assertEquals(1, parkingBoy.getParkingLots()[0].getParkedCars().size());
        parkingBoy.helpParkCar(new Car(2));
        assertEquals(2, parkingBoy.getParkingLots()[0].getParkedCars().size());
        assertEquals(0, parkingBoy.getParkingLots()[1].getParkedCars().size());

    }

    @Test
    void should_return_ticket_when_park_given_two_parking_lots_and_parking_boy_and_car_and_first_full_second_available() {
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot(), new ParkingLot());
        for (int i = 1; i <= parkingBoy.getParkingLots()[0].getParkSpace(); i++) {
            parkingBoy.helpParkCar(new Car(i));
        }

        parkingBoy.helpParkCar(new Car(11));
        assertEquals(10, parkingBoy.getParkingLots()[0].getParkedCars().size());
        assertEquals(1, parkingBoy.getParkingLots()[1].getParkedCars().size());

    }

    @Test
    void should_return_two_cars_when_fetch_given_two_parking_lots_and_parking_boy_and_two_car_parked() {
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot(), new ParkingLot());
        Car car_1 = new Car(1);
        Car car_2 = new Car(2);
        ParkingTicket ticket_1 = parkingBoy.getParkingLots()[0].park(car_1);
        ParkingTicket ticket_2 = parkingBoy.getParkingLots()[1].park(car_2);

        Car fetchedCar_1 = parkingBoy.helpFetchCar(ticket_1);
        Car fetchedCar_2 = parkingBoy.helpFetchCar(ticket_2);

        assertEquals(car_1, fetchedCar_1);
        assertEquals(car_2, fetchedCar_2);
    }

    @Test
    void should_return_nothing_with_error_message_when_fetch_given_two_parking_lots_and_parking_boy_and_wrong_ticket() {
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot(), new ParkingLot());
        parkingBoy.getParkingLots()[0].park(new Car(1));
        parkingBoy.getParkingLots()[1].park(new Car(2));

        var exception = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingBoy.helpFetchCar(new ParkingTicket(new Car(3))));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_return_nothing_with_error_message_when_fetch_given_two_parking_lots_and_parking_boy_and_used_ticket() {
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot(), new ParkingLot());
        ParkingTicket ticket = parkingBoy.helpParkCar(new Car(1));
        parkingBoy.helpFetchCar(ticket);

        var exception = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingBoy.helpFetchCar(ticket));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_two_full_parking_lots_and_parking_boy_and_car() {
        //given
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot(), new ParkingLot());
        Arrays.stream(parkingBoy.getParkingLots()).forEach(sparkingLot -> {
            for (int i = 1; i <= sparkingLot.getParkSpace(); i++) {
                parkingBoy.helpParkCar(new Car(i));
            }
        });

        //then
        var exception = assertThrows(NoAvailablePositionException.class, () -> parkingBoy.helpParkCar(new Car(11)));
        assertEquals("No available position.", exception.getMessage());
    }
}
