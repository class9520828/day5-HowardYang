package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ParkingLotServiceManagerTest {
    @Test
    void should_add_parking_boy_to_list_when_add_given_manager_multiple_parking_boys() {
        ParkingLotServiceManager serviceManager = new ParkingLotServiceManager();
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot());
        serviceManager.addManagedParkingBoy(parkingBoy);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(new ParkingLot());
        serviceManager.addManagedParkingBoy(smartParkingBoy);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(new ParkingLot());
        serviceManager.addManagedParkingBoy(superSmartParkingBoy);

        assertEquals(parkingBoy, serviceManager.getManagingParkingBoys().get(0));
        assertEquals(smartParkingBoy, serviceManager.getManagingParkingBoys().get(1));
        assertEquals(superSmartParkingBoy, serviceManager.getManagingParkingBoys().get(2));
        assertEquals(3, serviceManager.getManagingParkingBoys().size());
    }

    @Test
    void should_return_ticket_when_specify_parking_boy_park_given_manager_multiple_parking_boys() {
        ParkingLotServiceManager serviceManager = new ParkingLotServiceManager();
        serviceManager.getManagingParkingBoys().add(new ParkingBoy(new ParkingLot()));
        serviceManager.getManagingParkingBoys().add(new SmartParkingBoy(new ParkingLot()));
        serviceManager.getManagingParkingBoys().add(new SuperSmartParkingBoy(new ParkingLot()));

        ParkingTicket ticket = serviceManager.specifyParkingBoyPark(0, new Car(1));

        assertNotNull(ticket);
        assertEquals(1, serviceManager.getManagingParkingBoys().get(0).getParkingLots()[0].getParkedCars().size());

    }

    @Test
    void should_return_car_when_specify_parking_boy_fetch_given_manager_multiple_parking_boys_ticket() {
        ParkingLotServiceManager serviceManager = new ParkingLotServiceManager();
        serviceManager.addManagedParkingBoy(new ParkingBoy(new ParkingLot()));
        Car car = new Car(1);
        ParkingTicket ticket = serviceManager.specifyParkingBoyPark(0, car);
        Car fetchedCar = serviceManager.specifyParkingBoyFetch(ticket);

        assertEquals(car, fetchedCar);

    }

    @Test
    void should_return_ticket_when_park_given_parking_lot_and_manager_and_car() {
        ParkingLotServiceManager serviceManager = new ParkingLotServiceManager(new ParkingLot());
        ParkingTicket ticket = serviceManager.helpParkCar(new Car(1));
        assertNotNull(ticket);

    }

    @Test
    void should_return_car_when_fetch_given_parking_lot_and_manager_and_ticket() {

        ParkingLotServiceManager serviceManager = new ParkingLotServiceManager(new ParkingLot());
        ParkingTicket ticket = serviceManager.helpParkCar(new Car(1));
        Car fetchedCar = serviceManager.helpFetchCar(ticket);
        assertNotNull(fetchedCar);

    }

}
