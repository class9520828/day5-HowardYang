package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SuperSmartParkingBoyTest {
    @Test
    void should_parked_in_lot_more_empty_rate_when_park_given_multiple_parking_lots_and_super_smart_parking_boy_and_car() {
        ParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(new ParkingLot(20), new ParkingLot(15), new ParkingLot(100));
        Arrays.stream(superSmartParkingBoy.getParkingLots())
                .forEach(sparkingLot -> {
                    for (int i = 1; i <= 10; i++) {
                        sparkingLot.park(new Car(i));
                    }
                });

        superSmartParkingBoy.helpParkCar(new Car(11));
        assertEquals(11, superSmartParkingBoy.getParkingLots()[2].getParkedCars().size());

    }

    @Test
    void should_return_car_when_fetch_given_two_parking_lots_and_super_smart_parking_boy_and_two_tickets() {
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(new ParkingLot(), new ParkingLot());
        //given
        Car car_1 = new Car(1);
        Car car_2 = new Car(2);
        ParkingTicket ticket_1 = superSmartParkingBoy.helpParkCar(car_1);
        ParkingTicket ticket_2 = superSmartParkingBoy.helpParkCar(car_2);

        //when
        Car fetched_car_1 = superSmartParkingBoy.helpFetchCar(ticket_1);
        Car fetched_car_2 = superSmartParkingBoy.helpFetchCar(ticket_2);

        //then
        assertEquals(car_1, fetched_car_1);
        assertEquals(car_2, fetched_car_2);

    }

    @Test
    void should_return_nothing_with_error_message_when_fetch_given_multiple_parking_lots_and_super_smart_parking_boy_and_wrong_ticket() {
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(new ParkingLot(), new ParkingLot(), new ParkingLot());
        superSmartParkingBoy.getParkingLots()[0].park(new Car(1));
        superSmartParkingBoy.getParkingLots()[1].park(new Car(2));

        var exception = assertThrows(UnrecognizedParkingTicketException.class, () -> superSmartParkingBoy.helpFetchCar(new ParkingTicket(null)));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());

    }

    @Test
    void should_return_nothing_with_error_message_when_fetch_given_multiple_parking_lots_and_super_smart_parking_boy_and_used_ticket() {
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(new ParkingLot(), new ParkingLot(), new ParkingLot());
        ParkingTicket ticket = superSmartParkingBoy.helpParkCar(new Car(1));
        superSmartParkingBoy.helpFetchCar(ticket);

        var exception = assertThrows(UnrecognizedParkingTicketException.class, () -> superSmartParkingBoy.helpFetchCar(ticket));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());

    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_multiple_full_parking_lots_and_super_smart_parking_boy_and_car() {
        //given
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(new ParkingLot(), new ParkingLot(), new ParkingLot());
        Arrays.stream(superSmartParkingBoy.getParkingLots()).forEach(sparkingLot -> {
            for (int i = 1; i <= sparkingLot.getParkSpace(); i++) {
                superSmartParkingBoy.helpParkCar(new Car(i));
            }
        });

        //then
        var exception = assertThrows(NoAvailablePositionException.class, () -> superSmartParkingBoy.helpParkCar(new Car(11)));
        assertEquals("No available position.", exception.getMessage());

    }


}
