package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotTest {
    @Test
    void should_return_ticket_when_park_given_parking_lot_and_car() {
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car(1);
        ParkingTicket ticket = parkingLot.park(car);

        assertNotNull(ticket);

    }

    @Test
    void should_return_car_when_fetch__given_parking_lot_and_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car(1);
        ParkingTicket ticket = parkingLot.park(car);

        //when
        Car fetchedCar = parkingLot.fetch(ticket);

        //then
        assertNotNull(fetchedCar);
    }

    @Test
    void should_return_the_car_when_fetch_given_parking_lot_and_two_tickets() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car_1 = new Car(1);
        Car car_2 = new Car(2);
        ParkingTicket ticket_1 = parkingLot.park(car_1);
        ParkingTicket ticket_2 = parkingLot.park(car_2);

        //when
        Car fetched_car_1 = parkingLot.fetch(ticket_1);
        Car fetched_car_2 = parkingLot.fetch(ticket_2);

        //then
        assertEquals(car_1, fetched_car_1);
        assertEquals(car_2, fetched_car_2);

    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_lot_and_wrong_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car(1);
        ParkingTicket wrong_ticket = new ParkingTicket(new Car(2));
        parkingLot.park(car);

        //then
        var exception = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingLot.fetch(wrong_ticket));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());

    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_lot_and_used_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car(1);
        ParkingTicket ticket = new ParkingTicket(car);
        parkingLot.park(car);

        //when
        parkingLot.fetch(ticket);

        //then
        var exception = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingLot.fetch(ticket));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());

    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_full_parking_lot_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        for (int i = 1; i <= parkingLot.getParkSpace(); i++) {
            parkingLot.park(new Car(i));
        }

        //then
        var exception = assertThrows(NoAvailablePositionException.class, () -> parkingLot.park(new Car(11)));
        assertEquals("No available position.", exception.getMessage());
    }

}
