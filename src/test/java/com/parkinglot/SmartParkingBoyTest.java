package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SmartParkingBoyTest {
    @Test
    void should_parked_in_lot_more_empty_when_park_given_multiple_parking_lots_and_smart_parking_boy_and_car() {
        ParkingBoy smartParkingBoy = new SmartParkingBoy(new ParkingLot(), new ParkingLot(), new ParkingLot());
        Arrays.stream(smartParkingBoy.getParkingLots()).forEach(sparkingLot -> {
            for (int i = 1; i <= 3; i++) {
                sparkingLot.park(new Car(i));
            }
        });

        smartParkingBoy.getParkingLots()[0].park(new Car(4));
        smartParkingBoy.getParkingLots()[2].park(new Car(4));

        smartParkingBoy.helpParkCar(new Car(4));

        assertEquals(4, smartParkingBoy.getParkingLots()[1].getParkedCars().size());

    }

    @Test
    void should_return_car_when_fetch_given_two_parking_lots_and_smart_parking_boy_and_two_tickets() {
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(new ParkingLot(), new ParkingLot());
        //given
        Car car_1 = new Car(1);
        Car car_2 = new Car(2);
        ParkingTicket ticket_1 = smartParkingBoy.helpParkCar(car_1);
        ParkingTicket ticket_2 = smartParkingBoy.helpParkCar(car_2);

        //when
        Car fetched_car_1 = smartParkingBoy.helpFetchCar(ticket_1);
        Car fetched_car_2 = smartParkingBoy.helpFetchCar(ticket_2);

        //then
        assertEquals(car_1, fetched_car_1);
        assertEquals(car_2, fetched_car_2);

    }

    @Test
    void should_return_nothing_with_error_message_when_fetch_given_multiple_parking_lots_and_smart_parking_boy_and_wrong_ticket() {
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(new ParkingLot(), new ParkingLot(), new ParkingLot());
        smartParkingBoy.getParkingLots()[0].park(new Car(1));
        smartParkingBoy.getParkingLots()[1].park(new Car(2));

        var exception = assertThrows(UnrecognizedParkingTicketException.class, () -> smartParkingBoy.helpFetchCar(new ParkingTicket(new Car(3))));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());

    }

    @Test
    void should_return_nothing_with_error_message_when_fetch_given_multiple_parking_lots_and_smart_parking_boy_and_used_ticket() {
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(new ParkingLot(), new ParkingLot(), new ParkingLot());
        ParkingTicket ticket = smartParkingBoy.helpParkCar(new Car(1));
        smartParkingBoy.helpFetchCar(ticket);

        var exception = assertThrows(UnrecognizedParkingTicketException.class, () -> smartParkingBoy.helpFetchCar(ticket));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());

    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_multiple_full_parking_lots_and_smart_parking_boy_and_car() {
        //given
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(new ParkingLot(), new ParkingLot(), new ParkingLot());
        Arrays.stream(smartParkingBoy.getParkingLots()).forEach(sparkingLot -> {
            for (int i = 1; i <= sparkingLot.getParkSpace(); i++) {
                smartParkingBoy.helpParkCar(new Car(i));
            }
        });

        //then
        var exception = assertThrows(NoAvailablePositionException.class, () -> smartParkingBoy.helpParkCar(new Car(11)));
        assertEquals("No available position.", exception.getMessage());

    }

}
